# Task 1
*** 

### Start Date : 09/10/2023 , End Date : 09/10/2023

* Create a folder named "Flask_Learning".
* Create a simple flask application that has a simple homepage (just a welcome screen).
* Document the procedure and add the code and documentation inside the "Flask_Learning" folder.

# Task 2
***

### Start Date :17/10/2023 , End Date : 19/10/2023

Go through the given link and learn how to access the chirpstack API, fetch data, and print it on the console.

https://tekworldthink.blogspot.com/2023/09/how-to-access-chirpstack-api-python.html

# Task 3
***

### Start Date : 10/10/2023 , End Date : 10/10/2023 

Create a simple HTML page that integrates OpenStreetMap. Add a marker in Greenfield Stadium using javascript, take screenshots, and upload them to a folder named "Open Street Map.".

# Task 4
***

### Start Date : 16/10/2023 , End Date : 16/10/2023 

Create a flask application and show an open street map on the home page.


# Task 5
***

### Start Date : 19/10/2023 , End Date : 19/10/2023 

* What is API?
* Purpose?
* API endpoints?
* API Use case?
* API data formats.

# Task 6
***

### Start Date : 20/10/2023 , End Date : 20/10/2023 

* HTTP resoponse codes and meanings.
* Authentication vs Authorization

# Task 7
***

### Start Date : 20/10/2023 , End Date : 20/10/2023 

* Write the code to fetch and print only the names of all the applications from the Chirpstack server

# Task 8
***

### Start Date : 25/10/2023 , End Date : 25/10/2023 

* Make a flask application that shows data into an html page fetched from the server

# Task 9
***

### Start Date : 25/10/2023 , End Date : 27/10/2023

* Develop a Flask application that incorporates an OpenStreetMap, and enhance it by displaying three coordinates on the map, along with fetching application data from "lorawandev.icfoss.org" and presenting this data at each of the specified coordinates."


# Task 10
***

### Start Date : 28/10/2023 , End Date : 

* Fetch the location details from the '/api/gateways' and display corresponding markers on the map.

# Task 11
***

### Start Date: 28/10/2023, End Date :

Make a documentation of the following:
* Client-Server Architecture
* URL
* URL Structure
* URI
* URL vs URI
* HTTP endpoints

# Task 12
*** 

### Start Date :           , End Date :

* Display the applications names from '/api/applications' for each marker created from the previous task.

