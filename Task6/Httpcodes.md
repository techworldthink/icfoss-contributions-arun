* 200 OK : This means the request was successful and the server has returned the requested content.

* 404 Not Found : This means the server couldnot found the requested resource. Its like saying " I looked everywhere but I couldnt find what you are looking for".

* 401 Unauthorized : This means you need proper authentication (eg:username and password) to access the resource.

* 403 Forbidden : This means you dont have permission to access the requested resource.

* 500 Internal Server Error : This means something went wrong on the server's side while processing your request.

* 302 Found : This indicates a redirection.

* 503 Service Unavailable : This means the server is temporarily unable to handle the request.

* 201 Created : This means the requets was successful, and a new request was Created as a result.

* 400 Bad Request : This means the server couldnt understand your request.

* 204 No Content : This means the request was successful but there's no new data to sent back.
